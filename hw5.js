"use strict";

let createNewUser = function () {
  let newUser = {
    _firstName: prompt("What is your first name?"),
    _lastName: prompt("What is your  last name?"),

    getLogin: function () {
      let result =
        this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
      return result;
    },
  };
  return newUser;
};

Object.defineProperty(createNewUser, "_firstName", {
  writable: false,
});
Object.defineProperty(createNewUser, "_lastName", {
  writable: false,
});

Object.defineProperty(createNewUser, "firstName", {
  get() {
    return this._firstName;
  },
  set(value) {
    this._firstName = value;
  },
});
Object.defineProperty(createNewUser, "lastName", {
  get() {
    return this._lastName;
  },
  set(value) {
    this._lastName = value;
  },
});

let firstUser = createNewUser().getLogin();
console.log(firstUser);
